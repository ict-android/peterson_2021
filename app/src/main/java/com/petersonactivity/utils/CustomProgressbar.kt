package com.app.smsautolineactivity.utils


import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.TextView
import com.petersonactivity.R


class CustomProgressbar : Dialog {
    private var mProgressbar: CustomProgressbar? = null
    private var mOnDissmissListener: DialogInterface.OnDismissListener? = null

    private constructor(cancelVisible: Boolean, context: Context?) : super(context!!) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_progressbar)
        val cancelAction = findViewById<TextView>(R.id.cancelAction)
        val progressHorizontal =
                findViewById<View>(R.id.progress_horizontal)
        if (cancelVisible) {
            cancelAction.visibility = View.VISIBLE
            progressHorizontal.visibility = View.VISIBLE
        } else {
            cancelAction.visibility = View.GONE
            progressHorizontal.visibility = View.GONE
        }
        cancelAction.setOnClickListener { v: View? -> hideProgressBar() }
        this.window!!.setBackgroundDrawableResource(R.color.transparent)
    }

    private constructor(context: Context) : super(context) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_progressbar)
        val cancelAction = findViewById<TextView>(R.id.cancelAction)
        cancelAction.setOnClickListener { v: View? -> hideProgressBar() }
        this.window!!.setBackgroundDrawableResource(R.color.transparent)
    }

    constructor(context: Context, instance: Boolean?) : super(context) {
        mProgressbar = CustomProgressbar(context)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (mOnDissmissListener != null) {
            mOnDissmissListener!!.onDismiss(this)
        }
    }

    private fun setListener(listener: DialogInterface.OnDismissListener) {
        mOnDissmissListener = listener
    }

    fun showProgress(context: Context?, cancelable: Boolean, message: String?) {
        if (mProgressbar != null && mProgressbar!!.isShowing) {
            mProgressbar!!.cancel()
        }
        mProgressbar!!.setCancelable(cancelable)
        mProgressbar!!.show()
    }

    companion object {
        private var mCustomProgressbar: CustomProgressbar? = null
        fun showProgressBar(context: Context?,cancelable: Boolean) {
            showProgressBar(context, cancelable, null, true)
        }

        fun showProgressBarNonStop(context: Context?,cancelable: Boolean) {
            showProgressBar(context, cancelable, null, false)
        }

        fun showProgressBarNonStopWithCancel(context: Context?, cancelable: Boolean) {
            showProgressBar(context, cancelable, null, true)
        }

        fun showProgressBar(context: Context, cancelable: Boolean, message: String?) {
            try {
                if (mCustomProgressbar != null && mCustomProgressbar!!.isShowing) {
                    mCustomProgressbar!!.cancel()
                    mCustomProgressbar = null
                }
                if (message != null) {
                }
                mCustomProgressbar = CustomProgressbar(context)
                mCustomProgressbar!!.setCancelable(cancelable)
                mCustomProgressbar!!.show()
            } catch (e: Exception) {
                e.message?.let { Log.e("Ex", it) }
            }
        }

        fun showProgressBar(context: Context?, cancelable: Boolean, message: String?, cancelVisible: Boolean) {
            try {
                if (mCustomProgressbar != null && mCustomProgressbar!!.isShowing) {
                    mCustomProgressbar!!.cancel()
                    mCustomProgressbar = null
                }
                if (message != null) {
                }
                mCustomProgressbar =
                        CustomProgressbar(cancelVisible, context)
                mCustomProgressbar!!.setCancelable(cancelable)
                mCustomProgressbar!!.show()
            } catch (e: Exception) {
                e.message?.let { Log.e("Ex", it) }
            }
        }

        fun showProgressBar( context: Context, listener: DialogInterface.OnDismissListener) {
            if (mCustomProgressbar != null && mCustomProgressbar!!.isShowing) {
                mCustomProgressbar!!.cancel()
                mCustomProgressbar = null
            }
            mCustomProgressbar = CustomProgressbar(context)
            mCustomProgressbar!!.setListener(listener)
            mCustomProgressbar!!.setCancelable(java.lang.Boolean.TRUE)
            mCustomProgressbar!!.show()
        }

        fun hideProgressBar() {
            try {
                if (mCustomProgressbar != null && mCustomProgressbar!!.isShowing) {
                    mCustomProgressbar!!.dismiss()
                }
            } catch (e: Exception) {
                e.message?.let { Log.e("Ex", it) }
            }
        }

        fun showListViewBottomProgressBar(view: View) {
            if (mCustomProgressbar != null) {
                mCustomProgressbar!!.dismiss()
            }
            view.visibility = View.VISIBLE
        }

        fun hideListViewBottomProgressBar(view: View) {
            if (mCustomProgressbar != null) {
                mCustomProgressbar!!.dismiss()
            }
            view.visibility = View.GONE
        }
    }
}