package com.app.smsautolineactivity.utils

import android.util.Log
import com.karumi.dexter.BuildConfig

object AppLogger {
    private const val LABEL = "#Logger "
    fun d(tag: String, string: String?) {
        if (BuildConfig.DEBUG) {
            if (string != null) {
                Log.d(LABEL + tag, string)
            }
        }
    }

    fun v(tag: String, string: String?) {
        if (BuildConfig.DEBUG) {
            if (string != null) {
                Log.v(LABEL + tag, string)
            }
        }
    }

    fun i(tag: String, string: String?) {
        if (BuildConfig.DEBUG) {
            if (string != null) {
                Log.i(LABEL + tag, string)
            }
        }
    }

    fun e(tag: String, string: String?) {
        if (BuildConfig.DEBUG) {
            if (string != null) {
                Log.e(LABEL + tag, string)
            }
        }
    }

    fun w(tag: String, string: String?) {
        if (BuildConfig.DEBUG) {
            if (string != null) {
                Log.w(LABEL + tag, string)
            }
        }
    }
}