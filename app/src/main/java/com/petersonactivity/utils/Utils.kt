package com.app.smsautolineactivity.utils

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import com.google.gson.Gson
import com.petersonactivity.request.ErrorBody
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


object Utils {

    var TAG = "two_hearts"

    var fileNameDateFormat = SimpleDateFormat("yyyyMMddHHmmss",
            Locale.getDefault())

    var serverDateFormat = SimpleDateFormat("yyyy-MM-dd",
            Locale.getDefault())

    var displayDateFormat = SimpleDateFormat("dd-MM-yyyy",
            Locale.getDefault())

    var displayTimeFormat = SimpleDateFormat("hh:mm aa",
            Locale.getDefault())

    var timestampFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
            Locale.getDefault())

    var timeFormat = SimpleDateFormat("hh:mm aa",
            Locale.getDefault())

    private val MY_PERMISSIONS_REQUEST_STORAGE = 123
    private val perms = arrayOf("android.permission.READ_EXTERNAL_STORAGE")


    /*public static List<String> getGradeStringList(List<Grade> gradeList) {

        Collections.sort(gradeList, new GradeComparator());

        List<String> gradeStringList = new ArrayList<>();

        for (Grade grade : gradeList) {

            gradeStringList.add(grade.getGradeName());

        }

        return gradeStringList;
    }*/

    fun showDialog(context: Context, dialogFragment: DialogFragment){

        val fragmentActivity = context as FragmentActivity

        dialogFragment.isCancelable = false
        dialogFragment.show(fragmentActivity.supportFragmentManager, "message_dialog")

        fragmentActivity.supportFragmentManager.executePendingTransactions()

        val window = dialogFragment.dialog?.window
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val width = (fragmentActivity.resources.displayMetrics.widthPixels * 0.90).toInt()
        //int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);
        window?.setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    val fileNameDateString: String
        get() = fileNameDateFormat.format(Calendar.getInstance().time)

    val todaysDateString: String
        get() = serverDateFormat.format(Calendar.getInstance().time)

    fun getErrorBody(response: Response<*>): ErrorBody {
        try {

            return Gson().fromJson(response.errorBody()?.charStream(), ErrorBody::class.java)
        } catch (e:Exception){
            return ErrorBody(500, "Server down for maintenance. Please try again later.")
        }
    }



    fun getDisplayDateFromTimestamp(timestamp: String): String {

        var date: Date

        try {

            date = timestampFormat.parse(timestamp)

        } catch (e: ParseException) {

            date = Calendar.getInstance().time
        }

        return displayDateFormat.format(date)

    }

    fun getTimeFromLong(longTime: Long): String {
        val date = Date()
        date.time = longTime
        return timeFormat.format(date)

    }

    fun getDisplayDateFromLong(longTime: Long): String {
        val date = Date()
        date.time = longTime
        return displayDateFormat.format(date)

    }

    fun getFormattedString(unformattedString: String?): String {

        return if (unformattedString == null || unformattedString.trim { it <= ' ' }.isEmpty())
            "N/A"
        else
            unformattedString
    }

    fun getDisplayDateString(unformattedDateString: String): String {
        var date: Date

        try {
            date = serverDateFormat.parse(unformattedDateString)
        } catch (e: ParseException) {
            date = Calendar.getInstance().time
        }

        return displayDateFormat.format(date)
    }

    fun getDisplayTimeString(unformattedDateString: String): String {
        var date: Date

        try {
            date = serverDateFormat.parse(unformattedDateString)
        } catch (e: ParseException) {
            date = Calendar.getInstance().time
        }

        return displayDateFormat.format(date)
    }

    fun checkIfValidDates(fromDateString: String, toDateString: String): Boolean {

        var fromDate: Date? = null
        var toDate: Date? = null
        try {
            fromDate = displayDateFormat.parse(fromDateString)
            toDate = displayDateFormat.parse(toDateString)

        } catch (e: ParseException) {
            e.printStackTrace()
        }

        assert(fromDate != null)
        return fromDate!!.before(toDate)
    }

    fun toJson(`object`: Any): String {
        val gson = Gson()
        return gson.toJson(`object`)
    }

    fun isValid(string: String?): Boolean {

        return !(string == null || string.trim().equals("null") || string.isBlank())
    }

    fun isValid(int: Int?): Boolean {

        return !(int == null || int == 0)
    }

    fun isValid(list : List<Any>?): Boolean {

        return !(list == null || list.isEmpty())
    }

    fun isValidEmail(target: String): Boolean {
        return !TextUtils.isEmpty(target.trim { it <= ' ' }) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }

    fun loge(message: String) {
        Log.e("$TAG ->", message)
    }

    fun logd(message: String) {
        Log.d("$TAG ->", message)
    }

    fun getProgDialog(context: Context): ProgressDialog {
        val progDialog = ProgressDialog(context)
        progDialog.setMessage("Loading...")
        progDialog.isIndeterminate = false
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progDialog.setCancelable(false)
        return progDialog
    }


    fun hasPermissions(context: Context, vararg permissions: String): Boolean {
        for (permission in permissions) {
            if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }

        return true
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    fun checkFilePermission(context: Context): Boolean {
        val currentAPIVersion = Build.VERSION.SDK_INT
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(context as Activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    val alertBuilder = AlertDialog.Builder(context)
                    alertBuilder.setCancelable(true)
                    alertBuilder.setTitle("Permission necessary")
                    alertBuilder.setMessage("Storage permission is necessary")
                    alertBuilder.setPositiveButton(android.R.string.yes) { dialog, which -> ActivityCompat.requestPermissions(context, perms, MY_PERMISSIONS_REQUEST_STORAGE) }
                    val alert = alertBuilder.create()
                    alert.show()

                } else {
                    ActivityCompat.requestPermissions(context, perms, MY_PERMISSIONS_REQUEST_STORAGE)
                }
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }

    fun defaultString(string: String?): String {
        return if (isValid(string))
            string!!
        else
            ""
    }


}
