package com.petersonactivity.response
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetAllCollectionResponse(
    @SerializedName("collections")
    val collections: List<Collection>?,
    @SerializedName("status")
    val status: Int
): Parcelable


@Parcelize
data class Collection(
    @SerializedName("collection_amount")
    val collectionAmount: String,
    @SerializedName("collection_id")
    val collectionId: String,
    @SerializedName("date")
    val date: String,
    @SerializedName("order_amount")
    val orderAmount: String,
    @SerializedName("order_id")
    val orderId: String,
    @SerializedName("order_no")
    val orderNo: String
): Parcelable