package com.petersonactivity.response
import com.google.gson.annotations.SerializedName


data class GetEmployeeDetailResponse(
    @SerializedName("address")
    val address: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("employee_id")
    val employeeId: String,
    @SerializedName("employee_no")
    val employeeNo: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("joining_date")
    val joiningDate: String,
    @SerializedName("mobile")
    val mobile: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("status")
    val status: Int
)