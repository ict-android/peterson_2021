package com.petersonactivity.response
import com.google.gson.annotations.SerializedName


data class GetCollectionDetailsResponse(
    @SerializedName("balance_amount")
    val balanceAmount: String,
    @SerializedName("collection_amount")
    val collectionAmount: String,
    @SerializedName("collection_id")
    val collectionId: String,
    @SerializedName("date")
    val date: String,
    @SerializedName("order_amount")
    val orderAmount: String,
    @SerializedName("status")
    val status: Int
)