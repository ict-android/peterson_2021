package com.petersonactivity.response
import com.google.gson.annotations.SerializedName


data class LoginResponse(
    @SerializedName("address")
    val address: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("employee_id")
    val employeeId: String,
    @SerializedName("employee_no")
    val employeeNo: String,
    @SerializedName("joining_date")
    val joiningDate: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("mobile")
    val mobile: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("status")
    val status: Int
)