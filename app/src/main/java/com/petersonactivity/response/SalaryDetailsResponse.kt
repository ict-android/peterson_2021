package com.petersonactivity.response
import com.google.gson.annotations.SerializedName


data class SalaryDetailsResponse(
    @SerializedName("salary")
    val salary: List<Salary>,
    @SerializedName("salary_balance")
    val salaryBalance: Int
)

data class Salary(
    @SerializedName("monthlyData")
    val monthlyData: List<MonthlyData>,
    @SerializedName("name")
    val name: String
)

data class MonthlyData(
    @SerializedName("balance")
    val balance: String,
    @SerializedName("date")
    val date: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("in")
    val inX: String,
    @SerializedName("out")
    val `out`: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("type")
    val type: Int
)