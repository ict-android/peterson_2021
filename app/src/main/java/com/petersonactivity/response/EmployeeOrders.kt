package com.petersonactivity.response
import com.google.gson.annotations.SerializedName

data class EmployeeOrders(
    @SerializedName("orders")
    var orders: ArrayList<EmployeeOrder>?,
    @SerializedName("status")
    var status: Int?
)

data class EmployeeOrder(
    @SerializedName("balance")
    var balance: String?,
    @SerializedName("customer_name")
    var customerName: String?,
    @SerializedName("date")
    var date: String?,
    @SerializedName("order_amount")
    var orderAmount: String?,
    @SerializedName("order_id")
    var orderId: String?,
    @SerializedName("order_no")
    var orderNo: String?,
    @SerializedName("status")
    var status: String?
)