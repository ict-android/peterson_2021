package com.petersonactivity.response
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
    data class OrderListResponse(
    @SerializedName("orders")
    val orders: List<Order>,
    @SerializedName("status")
    val status: Int
): Parcelable
@Parcelize
data class Order(
    @SerializedName("advance_amount")
    val advanceAmount: String,
    @SerializedName("balance_amount")
    val balanceAmount: String,
    @SerializedName("color")
    val color: String,
    @SerializedName("customer_name")
    val customerName: String,
    @SerializedName("date")
    val date: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("discount_amount")
    val discountAmount: String,
    @SerializedName("order_amount")
    val orderAmount: String,
    @SerializedName("order_id")
    val orderId: String,
    @SerializedName("order_no")
    val orderNo: String,
    @SerializedName("order_status")
    val orderStatus: String,
    @SerializedName("per_sq_feet_amt")
    val perSqFeetAmt: String,
    @SerializedName("received_amount")
    val receivedAmount: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("work_sq_feet")
    val workSqFeet: String
): Parcelable
