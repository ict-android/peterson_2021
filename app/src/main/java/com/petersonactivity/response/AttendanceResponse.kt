package com.petersonactivity.response
import com.google.gson.annotations.SerializedName


data class AttendanceResponse(
    @SerializedName("attendance")
    val attendance: Attendance
)

data class Attendance(
    @SerializedName("monthlyData")
    val monthlyData: List<MonthlyDatas>,
    @SerializedName("name")
    val name: String
)

data class MonthlyDatas(
    @SerializedName("1")
    val x1: Int,
    @SerializedName("10")
    val x10: Int,
    @SerializedName("11")
    val x11: Int,
    @SerializedName("12")
    val x12: Int,
    @SerializedName("13")
    val x13: Int,
    @SerializedName("14")
    val x14: Int,
    @SerializedName("15")
    val x15: Int,
    @SerializedName("16")
    val x16: Int,
    @SerializedName("17")
    val x17: Int,
    @SerializedName("18")
    val x18: Int,
    @SerializedName("19")
    val x19: Int,
    @SerializedName("2")
    val x2: Int,
    @SerializedName("20")
    val x20: Int,
    @SerializedName("21")
    val x21: Int,
    @SerializedName("22")
    val x22: Int,
    @SerializedName("23")
    val x23: Int,
    @SerializedName("24")
    val x24: Int,
    @SerializedName("25")
    val x25: Int,
    @SerializedName("26")
    val x26: Int,
    @SerializedName("27")
    val x27: Int,
    @SerializedName("28")
    val x28: Int,
    @SerializedName("29")
    val x29: Int,
    @SerializedName("3")
    val x3: Int,
    @SerializedName("30")
    val x30: Int,
    @SerializedName("31")
    val x31: Int,
    @SerializedName("4")
    val x4: Int,
    @SerializedName("5")
    val x5: Int,
    @SerializedName("6")
    val x6: Int,
    @SerializedName("7")
    val x7: Int,
    @SerializedName("8")
    val x8: Int,
    @SerializedName("9")
    val x9: Int
)