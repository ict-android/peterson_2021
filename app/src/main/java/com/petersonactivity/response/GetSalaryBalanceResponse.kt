package com.petersonactivity.response
import com.google.gson.annotations.SerializedName


data class GetSalaryBalanceResponse(
    @SerializedName("message")
    val message: String,
    @SerializedName("salary_balance")
    val salaryBalance: Int,
    @SerializedName("status")
    val status: Int
)