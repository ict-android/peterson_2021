package com.petersonactivity.response
import com.google.gson.annotations.SerializedName


data class OrderDetailResponse(
    @SerializedName("advance")
    val advance: String,
    @SerializedName("amount")
    val amount: String,
    @SerializedName("balance")
    val balance: String,
    @SerializedName("customer_name")
    val customerName: String,
    @SerializedName("date")
    val date: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("order_id")
    val orderId: String,
    @SerializedName("order_no")
    val orderNo: String,
    @SerializedName("status")
    val status: Int,
    @SerializedName("type")
    val type: String
)