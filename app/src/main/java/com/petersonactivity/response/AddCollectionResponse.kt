package com.petersonactivity.response
import com.google.gson.annotations.SerializedName


data class AddCollectionResponse(
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int
)