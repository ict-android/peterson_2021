package com.petersonactivity.response
import com.google.gson.annotations.SerializedName


data class UpdateCollectionResponse(
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int
)