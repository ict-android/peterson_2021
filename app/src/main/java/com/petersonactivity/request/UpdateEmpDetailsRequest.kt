package com.petersonactivity.request
import com.google.gson.annotations.SerializedName


data class UpdateEmpDetailsRequest(
    @SerializedName("address")
    val address: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("employee_id")
    val employeeId: String,
    @SerializedName("mobile")
    val mobile: String,
    @SerializedName("name")
    val name: String
)