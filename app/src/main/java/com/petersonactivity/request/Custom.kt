package com.petersonactivity.request

data class Custom(
    val name: String,
    val Image: Int,
    val cls: Class<*>?
)