package com.petersonactivity.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ErrorBody (

    @SerializedName("status")
    @Expose
    var status: Int = 0,
    @SerializedName("message")
    @Expose
    var message: String? = null

)
