package com.petersonactivity.request
import com.google.gson.annotations.SerializedName


data class ChangeEmployeePasswordRequest(
    @SerializedName("current_password")
    val currentPassword: String,
    @SerializedName("employee_id")
    val employeeId: String,
    @SerializedName("new_password")
    val newPassword: String
)