package com.petersonactivity.request
import com.google.gson.annotations.SerializedName


data class SalaryDetailsRequest(
    @SerializedName("employee_id")
    val employeeId: String
)