package com.petersonactivity.request
import com.google.gson.annotations.SerializedName


data class AttendanceRequest(
    @SerializedName("employee_id")
    val employeeId: String
)