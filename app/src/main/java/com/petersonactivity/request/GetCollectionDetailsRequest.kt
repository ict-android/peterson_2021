package com.petersonactivity.request
import com.google.gson.annotations.SerializedName


data class GetCollectionDetailsRequest(
    @SerializedName("collection_id")
    val collectionId: String
)