package com.petersonactivity.request
import com.google.gson.annotations.SerializedName


data class GetSalaryBalanceRequest(
    @SerializedName("employee_id")
    val employeeId: String
)