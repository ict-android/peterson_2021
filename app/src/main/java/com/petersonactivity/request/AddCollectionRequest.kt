package com.petersonactivity.request
import com.google.gson.annotations.SerializedName


data class AddCollectionRequest(
    @SerializedName("amount")
    val amount: String?,
    @SerializedName("date")
    val date: String?,
    @SerializedName("employee_id")
    val employeeId: String?,
    @SerializedName("order_id")
    val orderId: String?
)