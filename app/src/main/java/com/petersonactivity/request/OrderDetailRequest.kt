package com.petersonactivity.request
import com.google.gson.annotations.SerializedName


data class OrderDetailRequest(
    @SerializedName("order_id")
    val orderId: String
)