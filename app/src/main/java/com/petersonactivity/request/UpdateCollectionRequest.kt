package com.petersonactivity.request
import com.google.gson.annotations.SerializedName


data class UpdateCollectionRequest(
    @SerializedName("amount")
    val amount: String?,
    @SerializedName("collection_id")
    val collectionId: String?,
    @SerializedName("date")
    val date: String?,
    @SerializedName("employee_id")
    val employeeId: String?,
    @SerializedName("order_id")
    val orderId: String?
)