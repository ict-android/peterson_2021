package com.petersonactivity.ui.Login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import com.app.smsautolineactivity.data_models.SessionManager
import com.petersonactivity.R
import kotlinx.android.synthetic.main.activity_logout.*

class LogoutActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_logout)
        yesButton.setOnClickListener {
            SessionManager(this).clear()
            val inte = Intent(this, LoginActivity::class.java)
            inte.addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TOP or
                        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(inte)
        }
        noButton.setOnClickListener {
            finish()
        }
        this.setFinishOnTouchOutside(false)
    }

}