package com.petersonactivity.ui.Login

import android.os.Bundle
import android.widget.Toast
import com.app.smsautolineactivity.data_models.SessionManager
import com.app.smsautolineactivity.service.RetrofitEngine
import com.app.smsautolineactivity.utils.CustomProgressbar
import com.petersonactivity.R
import com.petersonactivity.ui.Base.BaseActivity
import com.petersonactivity.request.ChangeEmployeePasswordRequest
import com.petersonactivity.response.ChangeEmployeePasswordResponse
import kotlinx.android.synthetic.main.activity_password_change.*
import kotlinx.android.synthetic.main.toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PasswordChangeActivity : BaseActivity() {
    private lateinit var sessionManager: SessionManager
    private var api = RetrofitEngine.retrofit
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password_change)
        toolbarTitle.text = "PassWord Change"
        sessionManager = SessionManager(this)
        clickListeners()
    }
    private fun clickListeners() {
        ok_button.setOnClickListener {
            when{
                enter_old.text.isNullOrEmpty()->{
                    enter_old.error = "Please enter old PassWord"
                }
                enter_new.text.isNullOrEmpty()->{
                    enter_new.error = "Please enter New PassWord"
                }
                else -> {
                    changeEmployeePassword()
                }
            }
        }
    }

    private fun changeEmployeePassword( ) {
        CustomProgressbar.showProgressBar(this,false)
        val call: Call<ChangeEmployeePasswordResponse> = api.changeEmployeePassword(ChangeEmployeePasswordRequest(enter_old.text.toString(),
            SessionManager(this@PasswordChangeActivity).employeeId.toString(),enter_new.text.toString()))
        call.enqueue(object : Callback<ChangeEmployeePasswordResponse> {
            override fun onResponse(call: Call<ChangeEmployeePasswordResponse>, response: Response<ChangeEmployeePasswordResponse>) {
                CustomProgressbar.hideProgressBar()
                val changeCustomerPasswordResponse = response.body()
                if (changeCustomerPasswordResponse != null) {
                    if (changeCustomerPasswordResponse.message.contains("Success")) {
                        Toast.makeText(this@PasswordChangeActivity, changeCustomerPasswordResponse.message, Toast.LENGTH_LONG).show()

                    } else {
                        Toast.makeText(this@PasswordChangeActivity, changeCustomerPasswordResponse.message, Toast.LENGTH_LONG).show()
                    }
                }else {
                    Toast.makeText(this@PasswordChangeActivity, "Invalid PassWord", Toast.LENGTH_LONG).show()
                }
            }

            override fun onFailure(call: Call<ChangeEmployeePasswordResponse>, t: Throwable) {
                CustomProgressbar.hideProgressBar()
                Toast.makeText(this@PasswordChangeActivity,getString(R.string.request_failed), Toast.LENGTH_LONG).show();
            }
        })
    }


}