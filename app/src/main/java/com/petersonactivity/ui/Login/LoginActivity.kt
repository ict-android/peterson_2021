package com.petersonactivity.ui.Login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.app.smsautolineactivity.data_models.SessionManager
import com.app.smsautolineactivity.service.RetrofitEngine
import com.app.smsautolineactivity.utils.CustomProgressbar
import com.petersonactivity.ui.MainActivity
import com.petersonactivity.R
import com.petersonactivity.request.LoginRequest
import com.petersonactivity.response.LoginResponse
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {
    private var user = 10
    private lateinit var sessionManager: SessionManager
    private var api = RetrofitEngine.retrofit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        sessionManager = SessionManager(this)
        clickListeners()
        backGroundColor()
    }
    fun backGroundColor() {
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.red_glow)
        window.navigationBarColor = ContextCompat.getColor(this, R.color.red_glow)
        window.setBackgroundDrawableResource(R.drawable.gradient_background)
    }
    private fun clickListeners() {
        btnLogin.setOnClickListener {
            when{
                userId.text.isNullOrEmpty()->{
                    userId.error = "Please enter User Name"
                }
                password.text.isNullOrEmpty()->{
                    password.error = "Please enter Password"
                }
                else -> {
                    Log.e("Error","${user++} 0-- ${password.text.toString()}")
                    Log.d("Error2",userId.text.toString()+ "--" + password.text.toString())
                    Log.i("Error3","${userId.text} -- ${password.text}")
                    doLogin(userId.text.toString(),password.text.toString())
                }
            }
        }
    }

    private fun doLogin(userName :String, password :String) {
        CustomProgressbar.showProgressBar(this,false)
        val call: Call<LoginResponse> = api.doLogin(LoginRequest(password,userName))
        call.enqueue(object : Callback<LoginResponse> {
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                CustomProgressbar.hideProgressBar()
                val loginResponse = response.body()
                if (loginResponse != null) {
                    if (loginResponse.message.contains("Success")) {
                        Toast.makeText(this@LoginActivity, loginResponse.message, Toast.LENGTH_LONG).show()
                        sessionManager.saveUserInfo(loginResponse.email,loginResponse.employeeId,loginResponse.address,loginResponse.mobile,loginResponse.employeeNo)
                        startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                        finish()
                    } else {
                        Toast.makeText(this@LoginActivity, loginResponse.message, Toast.LENGTH_LONG).show()
                    }
                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                CustomProgressbar.hideProgressBar()
                Toast.makeText(this@LoginActivity,getString(R.string.request_failed), Toast.LENGTH_LONG).show();
            }
        })
    }
}