package com.petersonactivity.ui.collections

import android.os.Bundle
import com.petersonactivity.R
import com.petersonactivity.ui.Base.BaseActivity

class CollectionEntryActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_collection_entry)
    }
}