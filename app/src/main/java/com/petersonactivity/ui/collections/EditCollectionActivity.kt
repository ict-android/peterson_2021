package com.petersonactivity.ui.collections

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.app.smsautolineactivity.data_models.SessionManager
import com.app.smsautolineactivity.service.RetrofitEngine
import com.app.smsautolineactivity.utils.AppLogger
import com.app.smsautolineactivity.utils.CustomProgressbar
import com.petersonactivity.R
import com.petersonactivity.request.SalaryDetailsRequest
import com.petersonactivity.request.UpdateCollectionRequest
import com.petersonactivity.ui.Base.BaseActivity
import com.petersonactivity.response.Collection
import com.petersonactivity.response.EmployeeOrder
import com.petersonactivity.response.UpdateCollectionResponse
import kotlinx.android.synthetic.main.activity_addcollection.*
import kotlinx.android.synthetic.main.activity_editcollection.*
import kotlinx.android.synthetic.main.activity_editcollection.balanceAmount
import kotlinx.android.synthetic.main.activity_editcollection.btn_submit
import kotlinx.android.synthetic.main.activity_editcollection.collection_amt_txt
import kotlinx.android.synthetic.main.activity_editcollection.customerName
import kotlinx.android.synthetic.main.activity_editcollection.dateOf
import kotlinx.android.synthetic.main.activity_editcollection.date_time_edt
import kotlinx.android.synthetic.main.activity_editcollection.equipmentContent
import kotlinx.android.synthetic.main.activity_editcollection.orderAmount
import kotlinx.android.synthetic.main.activity_editcollection.searchAutoComplete
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class EditCollectionActivity : BaseActivity(){

    private lateinit var sessionManager: SessionManager
    private var api = RetrofitEngine.retrofit
    val calendar = Calendar.getInstance()
    var collectionOpen : Collection? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editcollection)
        collectionOpen = intent.getParcelableExtra("CollectionData")
        updateLabel()
        sessionManager = SessionManager(this)
        toolbarTitle.text = "Edit Collection"
        clickListener()
        orderDetails()
        Log.e("pushpa",collectionOpen.toString())
        collectionOpen?.let {
            collection_amt_txt.setText(collectionOpen?.collectionAmount)
            date_time_edt.text = collectionOpen?.date
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun spinnerSetup(equipments: ArrayList<EmployeeOrder>) {
        for (equipment in equipments){
            if(equipment.orderId == collectionOpen?.orderId){
                setInfo(equipment)
            }
        }
        searchAutoComplete.showSoftInputOnFocus = false
        val adapter = AutoCompleteAdapter(
            this, android.R.layout.simple_dropdown_item_1line,
            equipments
        )
        searchAutoComplete.setAdapter(adapter)
        searchAutoComplete.setOnItemClickListener { parent, view, position, id ->
            setInfo(equipments[position])
        }
        searchAutoComplete.setOnTouchListener { v, event ->
            searchAutoComplete.showDropDown()
            true
        }

    }

    var orderSelected : EmployeeOrder? = null
    private fun setInfo(collection: EmployeeOrder) {
        equipmentContent.visibility = View.VISIBLE
        orderSelected = collection
        searchAutoComplete.setText("${collection.orderId} - ${collection.customerName} - ${collection.orderAmount}")
        customerName.text=collection.customerName
        orderAmount.text=collection.orderAmount
        balanceAmount.text = collection.balance
        dateOf.text = collection.date
    }


    private fun addAllCollection() {
        CustomProgressbar.showProgressBar(this,false)
        val sdf = SimpleDateFormat("MM/dd/yyyy", Locale.US)
        val call: Call<UpdateCollectionResponse> = api.updateCollection(
            UpdateCollectionRequest(collection_amt_txt.text.toString(), collectionOpen?.collectionId.toString(),
                sdf.format(calendar.time), SessionManager(this).employeeId.toString(),
                orderSelected?.orderId))
        call.enqueue(object : Callback<UpdateCollectionResponse> {
            override fun onResponse(call: Call<UpdateCollectionResponse>, response: Response<UpdateCollectionResponse>) {
                CustomProgressbar.hideProgressBar()
                val addCollectionResponse = response.body()
                if (addCollectionResponse != null) {
                    if (addCollectionResponse.message.contains("Success")) {
                        finish()
                        Toast.makeText(this@EditCollectionActivity, addCollectionResponse.message, Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(this@EditCollectionActivity, addCollectionResponse.message, Toast.LENGTH_LONG).show()
                    }
                }
            }

            override fun onFailure(call: Call<UpdateCollectionResponse>, t: Throwable) {
                CustomProgressbar.hideProgressBar()
                Toast.makeText(this@EditCollectionActivity,getString(R.string.request_failed), Toast.LENGTH_LONG).show();
            }
        })
    }

    private fun clickListener() {
        date_time_edt.setOnClickListener {
            DatePickerDialog(
                this, date, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),calendar.get(
                    Calendar.DAY_OF_MONTH)
            ).show()
        }

        btn_submit.setOnClickListener {
            when{
                collection_amt_txt.text.toString().isEmpty()->{
                    collection_amt_txt.error = "Please enter address"
                }
                date_time_edt.text.toString().equals("Choose", true) -> {
                    Toast.makeText(this, "Please select date", Toast.LENGTH_LONG).show()
                }
                else -> {
                    addAllCollection()
                }
            }
        }
    }


    var date = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, monthOfYear)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        updateLabel()
    }

    private fun updateLabel() {
        val sdf = SimpleDateFormat("MMM dd, yyyy", Locale.US)
        date_time_edt.text = sdf.format(calendar.time)
    }

    private fun orderDetails() {
        CustomProgressbar.showProgressBar(this, false)
        CoroutineScope(Dispatchers.IO).launch {
            SessionManager(this@EditCollectionActivity).employeeId?.let { cu->
                val request = api.getEmployeeOrders(SalaryDetailsRequest(cu))
                withContext(Dispatchers.Main) {
                    try {
                        val response = request.await()
                        if (response.isSuccessful) {
                            CustomProgressbar.hideProgressBar()
                            response.body()?.let {
                                AppLogger.e("ResponseData", it.toString())
                                it.orders?.let { orders ->
                                    spinnerSetup(orders)
                                }
                            }
                        } else {
                            CustomProgressbar.hideProgressBar()
                        }
                    } catch (e: HttpException) {
                        e.printStackTrace()
                    } catch (e: Throwable) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }

}