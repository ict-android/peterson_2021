package com.petersonactivity.ui.collections

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.petersonactivity.R
import com.petersonactivity.response.Collection
import kotlinx.android.synthetic.main.activity_addcollection.view.*

class SpinnerAdapter(private val myDataset: ArrayList<Collection>,
                     val context: Context
) :
    RecyclerView.Adapter<SpinnerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.activity_addcollection, parent, false))
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataInfo = myDataset[position]

        // holder.equipment.textAlignment =dataInfo.equipment
        //holder.equipment_id.textAlignment=dataInfo.equipmentId

    }
    override fun getItemCount() = myDataset.size

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val  collectionId= view.spinnerContent


    }
}
