package com.petersonactivity.ui.collections

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.app.smsautolineactivity.data_models.SessionManager
import com.app.smsautolineactivity.service.RetrofitEngine
import com.app.smsautolineactivity.utils.AppLogger
import com.app.smsautolineactivity.utils.CustomProgressbar
import com.petersonactivity.R
import com.petersonactivity.ui.Base.BaseActivity
import com.petersonactivity.request.AddCollectionRequest
import com.petersonactivity.request.SalaryDetailsRequest
import com.petersonactivity.response.AddCollectionResponse
import com.petersonactivity.response.EmployeeOrder
import kotlinx.android.synthetic.main.activity_addcollection.*
import kotlinx.android.synthetic.main.activity_addcollection.balanceAmount
import kotlinx.android.synthetic.main.activity_addcollection.btn_submit
import kotlinx.android.synthetic.main.activity_addcollection.collection_amt_txt
import kotlinx.android.synthetic.main.activity_addcollection.customerName
import kotlinx.android.synthetic.main.activity_addcollection.dateOf
import kotlinx.android.synthetic.main.activity_addcollection.date_time_edt
import kotlinx.android.synthetic.main.activity_addcollection.equipmentContent
import kotlinx.android.synthetic.main.activity_addcollection.orderAmount
import kotlinx.android.synthetic.main.activity_addcollection.searchAutoComplete
import kotlinx.android.synthetic.main.activity_editcollection.*
import kotlinx.android.synthetic.main.list_collection.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class AddCollectionActivity : BaseActivity() {

    private lateinit var sessionManager: SessionManager
    private var api = RetrofitEngine.retrofit
    val calendar = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addcollection)
        sessionManager = SessionManager(this)
        toolbarTitle.text = "Add Collection"
        updateLabel()
        clickListener()
        orderDetails()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun spinnerSetup(equipments: ArrayList<EmployeeOrder>) {
        searchAutoComplete.showSoftInputOnFocus = false
        val adapter = AutoCompleteAdapter(
            this, android.R.layout.simple_dropdown_item_1line,
            equipments
        )
        searchAutoComplete.setAdapter(adapter)
        searchAutoComplete.setOnItemClickListener { parent, view, position, id ->
            setInfo(equipments[position])
        }
        searchAutoComplete.setOnTouchListener { v, event ->
            searchAutoComplete.showDropDown()
            true
        }

    }

    var orderSelected : EmployeeOrder? = null
    private fun setInfo(collection: EmployeeOrder) {
        equipmentContent.visibility = View.VISIBLE
        orderSelected = collection
        searchAutoComplete.setText("${collection.orderId} - ${collection.customerName} - ${collection.orderAmount}")
        customerName.text=collection.customerName
        orderAmount.text=collection.orderAmount
        balanceAmount.text = collection.balance
        dateOf.text = collection.date
    }

    private fun addAllCollection() {
        CustomProgressbar.showProgressBar(this,false)
        val sdf = SimpleDateFormat("MM/dd/yyyy", Locale.US)
        val objectName = AddCollectionRequest(collection_amt_txt.text.toString(),
                sdf.format(calendar.time), SessionManager(this@AddCollectionActivity).employeeId.toString(),
                orderSelected?.orderId)
        val call: Call<AddCollectionResponse> = api.addCollection(

         objectName

        )
        call.enqueue(object : Callback<AddCollectionResponse> {
            override fun onResponse(call: Call<AddCollectionResponse>, response: Response<AddCollectionResponse>) {
                CustomProgressbar.hideProgressBar()
                val addCollectionResponse = response.body()
                if (addCollectionResponse != null) {
                    if (addCollectionResponse.message.contains("Success")) {
                        finish()
                        Toast.makeText(this@AddCollectionActivity, addCollectionResponse.message, Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(this@AddCollectionActivity, addCollectionResponse.message, Toast.LENGTH_LONG).show()
                    }
                }
            }

            override fun onFailure(call: Call<AddCollectionResponse>, t: Throwable) {
                CustomProgressbar.hideProgressBar()
                Toast.makeText(this@AddCollectionActivity,getString(R.string.request_failed), Toast.LENGTH_LONG).show();
            }
        })
    }

    private fun clickListener() {
        date_time_edt.setOnClickListener {
            DatePickerDialog(
                this, date, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),calendar.get(
                    Calendar.DAY_OF_MONTH)
            ).show()
        }

        btn_submit.setOnClickListener {
            when{
                collection_amt_txt.text.toString().isEmpty()->{
                    collection_amt_txt.error = "Please enter address"
                }
                date_time_edt.text.toString().equals("Choose", true) -> {
                    Toast.makeText(this, "Please select date", Toast.LENGTH_LONG).show()
                }
                else -> {
                    addAllCollection()
                }
            }
        }
    }


    var date = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, monthOfYear)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        updateLabel()
    }

    private fun updateLabel() {
        val sdf = SimpleDateFormat("MMM dd, yyyy", Locale.US)
        date_time_edt.text = sdf.format(calendar.time)
    }

    private fun orderDetails() {
        CustomProgressbar.showProgressBar(this, false)
        CoroutineScope(Dispatchers.IO).launch {
            SessionManager(this@AddCollectionActivity).employeeId?.let { cu->
                val request = api.getEmployeeOrders(SalaryDetailsRequest(cu))
                withContext(Dispatchers.Main) {
                    try {
                        val response = request.await()
                        if (response.isSuccessful) {
                            CustomProgressbar.hideProgressBar()
                            response.body()?.let {
                                AppLogger.e("ResponseData", it.toString())
                                it.orders?.let { orders ->
                                    spinnerSetup(orders)
                                }
                            }
                        } else {
                            CustomProgressbar.hideProgressBar()
                        }
                    } catch (e: HttpException) {
                        e.printStackTrace()
                    } catch (e: Throwable) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }

}