package com.petersonactivity.ui.collections

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.petersonactivity.R
import com.petersonactivity.response.Collection
import kotlinx.android.synthetic.main.list_collection.view.*

class ListCollectionAdapter(private val myDataset: ArrayList<Collection>, val context: Context) :
    RecyclerView.Adapter<ListCollectionAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_collection, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataInfo = myDataset[position]
        holder.orderNo.text ="Collection ID: ${dataInfo.collectionId} / Order No: ${dataInfo.orderNo}"
        holder.date.text = "Date : ${dataInfo.date}"
        holder.orderAmount.text = "Order Amount : ${dataInfo.collectionAmount}"
        holder.layout.setOnClickListener {
            val intentVar = Intent(context, EditCollectionActivity::class.java)
            intentVar.putExtra("CollectionData",dataInfo)
            context.startActivity(intentVar)
        }
    }

    override fun getItemCount() = myDataset.size

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {

        val orderNo = view.order_no
        val orderAmount =view.amount
        val date =view.date_id
        val layout=view.layout
    }
}