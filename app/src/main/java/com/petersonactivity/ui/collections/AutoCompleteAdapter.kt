package com.petersonactivity.ui.collections

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import com.petersonactivity.R
import com.petersonactivity.response.EmployeeOrder


class AutoCompleteAdapter(context: Context, resource: Int, objects: List<EmployeeOrder>) :
    ArrayAdapter<EmployeeOrder>(context, resource, objects), Filterable {
    private var fullList: ArrayList<EmployeeOrder>
    private var mOriginalValues: ArrayList<EmployeeOrder>?
    private var mFilter: ArrayFilter? = null
    override fun getCount(): Int {
        return fullList.size
    }

    override fun getItem(position: Int): EmployeeOrder {
        return fullList[position]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var v = convertView
        if (v == null) {
            val vi = context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE
            ) as LayoutInflater
            v = vi.inflate(R.layout.spinner_item, null)
        }
        val product = fullList[position]
        val txtTitle = v?.findViewById(R.id.txtTitle) as TextView
        txtTitle.text = "${product.orderId} - ${product.customerName} - ${product.orderAmount}"
        return v
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        var v = convertView
        if (v == null) {
            val vi = context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE
            ) as LayoutInflater
            v = vi.inflate(R.layout.spinner_item, null)
        }
        val product = fullList[position]
        val txtTitle = v?.findViewById(R.id.txtTitle) as TextView
        txtTitle.text = "${product.orderId} - ${product.customerName} - ${product.orderAmount}"
        return v
    }

    override fun getFilter(): Filter {
        if (mFilter == null) {
            mFilter = ArrayFilter()
        }
        return mFilter as ArrayFilter
    }

    private inner class ArrayFilter : Filter() {
        override fun performFiltering(prefix: CharSequence?): FilterResults {
            val results = FilterResults()
            val list = ArrayList(mOriginalValues)
            results.values = list
            results.count = list.size
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults) {
            fullList = if (results.values != null) {
                results.values as ArrayList<EmployeeOrder>
            } else {
                ArrayList()
            }
            if (results.count > 0) {
                notifyDataSetChanged()
            } else {
                notifyDataSetInvalidated()
            }
        }
    }

    init {
        fullList = objects as ArrayList<EmployeeOrder>
        mOriginalValues = ArrayList(fullList)
    }
}