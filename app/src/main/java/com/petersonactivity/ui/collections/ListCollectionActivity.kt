package com.petersonactivity.ui.collections

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.smsautolineactivity.data_models.SessionManager
import com.app.smsautolineactivity.service.RetrofitEngine
import com.app.smsautolineactivity.utils.AppLogger
import com.app.smsautolineactivity.utils.CustomProgressbar
import com.petersonactivity.R
import com.petersonactivity.ui.Base.BaseActivity
import com.petersonactivity.request.GetAllCollectionRequest
import com.petersonactivity.response.Collection
import kotlinx.android.synthetic.main.activity_listcollection.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ListCollectionActivity : BaseActivity() {

    private var api = RetrofitEngine.retrofit
    val dataSetList = ArrayList<Collection>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listcollection)
        toolbarTitle.text = "List Collection"
        setUpRecycler()
        clickListener()
    }

    override fun onResume() {
        super.onResume()
        getAllCollections()
    }

    private fun clickListener() {
        addComplaint.setOnClickListener {
            startActivity(Intent(this, AddCollectionActivity::class.java))
        }
    }

    private fun setUpRecycler() {
        recycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycler.adapter = ListCollectionAdapter(dataSetList, this)
        (recycler.adapter as ListCollectionAdapter).notifyDataSetChanged()
    }

    private fun getAllCollections() {
        CustomProgressbar.showProgressBar(this, false)
        CoroutineScope(Dispatchers.IO).launch {
            SessionManager(this@ListCollectionActivity).employeeId?.let { cu->
                val request = api.getAllCollections(GetAllCollectionRequest(cu))
                withContext(Dispatchers.Main) {
                    try {
                        val response = request.await()
                        if (response.isSuccessful) {
                            CustomProgressbar.hideProgressBar()
                            response.body()?.let {
                                AppLogger.e("ResponseData", it.toString())
                                it.collections?.let{collections->
                                    if(collections.isEmpty()){
                                        noDataFound.visibility = View.VISIBLE
                                    } else {
                                        noDataFound.visibility = View.GONE
                                        dataSetList.clear()
                                        dataSetList.addAll(collections)
                                        (recycler.adapter as ListCollectionAdapter).notifyDataSetChanged()
                                    }
                                }
                            }
                        } else {
                            noDataFound.visibility = View.VISIBLE
                            CustomProgressbar.hideProgressBar()
                        }
                    } catch (e:Exception ) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }
}