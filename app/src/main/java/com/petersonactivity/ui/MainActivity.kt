package com.petersonactivity.ui

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.app.smsautolineactivity.data_models.SessionManager
import com.petersonactivity.*
import com.petersonactivity.ui.Attendance.AttendanceActivity
import com.petersonactivity.ui.Base.BaseActivity
import com.petersonactivity.ui.collections.ListCollectionActivity
import com.petersonactivity.ui.Login.LoginActivity
import com.petersonactivity.ui.Order.OrdersActivity
import com.petersonactivity.ui.Profile.ProfileActivity
import com.petersonactivity.ui.Login.PasswordChangeActivity
import com.petersonactivity.ui.salary.SalaryActivity
import com.petersonactivity.request.Custom
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_activity.*
import kotlinx.android.synthetic.main.nav_header.*
import kotlinx.android.synthetic.main.toolbar.*
import java.util.ArrayList

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        clickListener()
        homeRecycler()
    }
    private fun homeRecycler() {
        rvHome.layoutManager = GridLayoutManager(this, 2)
        rvHome.adapter = CustomAdapter(this,getHomeContent())
    }
    private fun getHomeContent(): ArrayList<Custom> {
        val homeContentList = ArrayList<Custom>()
        homeContentList.add(Custom("Orders", R.drawable.orders, OrdersActivity::class.java))
        homeContentList.add(Custom("Collection Entry", R.drawable.collection_entry, ListCollectionActivity::class.java))
        homeContentList.add(Custom("Profile", R.drawable.profile,ProfileActivity::class.java))
        homeContentList.add(Custom("Attendance",R.drawable.attendence_psd, AttendanceActivity::class.java))
        homeContentList.add(Custom("Salary",R.drawable.salary, SalaryActivity::class.java))
        return homeContentList
    }

    @SuppressLint("WrongConstant")
    private fun clickListener() {
        navigationDrawer.setOnClickListener {
            if(!drawerLayout.isDrawerOpen(Gravity.START)){
                drawerLayout.openDrawer(Gravity.START)
            }else {
                drawerLayout.closeDrawer(Gravity.END)
            }
        }
        txt_home.setOnClickListener {
            startActivity(Intent(this, OrdersActivity::class.java))
        }
        txt_transa.setOnClickListener {
            startActivity(Intent(this, ListCollectionActivity::class.java))
        }
        txt_bills.setOnClickListener {
            startActivity(Intent(this, AttendanceActivity::class.java))
        }
        equipment_details.setOnClickListener {
            startActivity(Intent(this, SalaryActivity::class.java))
        }

        txt_change_password.setOnClickListener {
            startActivity(Intent(this, PasswordChangeActivity::class.java))
        }
        txt_invite.setOnClickListener {
            SessionManager(this).clear()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
        txt_cash_transaction.setOnClickListener {
            startActivity(Intent(this, ProfileActivity::class.java))
        }


        txt_invite.setOnClickListener {
            val mAlertDialogBuilder = AlertDialog.Builder(this@MainActivity)
            mAlertDialogBuilder.setTitle("Peterson")
            mAlertDialogBuilder.setIcon(R.drawable.ic_baseline_add_24)
            mAlertDialogBuilder.setMessage("Are you sure do you want to Exit ?")
            mAlertDialogBuilder.setCancelable(false)
            mAlertDialogBuilder.setPositiveButton("Yes"){_,_->
                SessionManager(this).clear()
                val inte = Intent(this, LoginActivity::class.java)
                inte.addFlags(
                    Intent.FLAG_ACTIVITY_CLEAR_TOP or
                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(inte)
            }
            mAlertDialogBuilder.setNegativeButton("No"){_,_->
                Toast.makeText(this@MainActivity,"clicked No Button", Toast.LENGTH_LONG).show()
            }
            mAlertDialogBuilder.setNeutralButton("Cancel"){_,_->
                Toast.makeText(this@MainActivity,"clicked Cancel Button", Toast.LENGTH_LONG).show()
            }
            val mAlertDialog=mAlertDialogBuilder.create()
            mAlertDialog.show()
        }

    }
    override fun onResume() {
        super.onResume()
        if(SessionManager(this).employeeId==null){
            val inte = Intent(this, LoginActivity::class.java)
            inte.addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TOP or
                        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(inte)
        }

    }
}