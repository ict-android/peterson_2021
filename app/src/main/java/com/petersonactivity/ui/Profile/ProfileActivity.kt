package com.petersonactivity.ui.Profile

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import com.app.smsautolineactivity.data_models.SessionManager
import com.app.smsautolineactivity.service.RetrofitEngine
import com.app.smsautolineactivity.utils.CustomProgressbar
import com.app.smsautolineactivity.utils.FileUtils
import com.bumptech.glide.Glide
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.petersonactivity.ui.imagePicker.ImagePickerActivity
import com.petersonactivity.R
import com.petersonactivity.ui.Base.BaseActivity
import com.petersonactivity.request.GetEmployeeDetailsRequest
import com.petersonactivity.request.UpdateEmpDetailsRequest
import com.petersonactivity.response.GetEmployeeDetailResponse
import com.petersonactivity.response.UpdateEmpDetailResponse
import kotlinx.android.synthetic.main.activity_addcollection.*
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

class ProfileActivity : BaseActivity() {
    private var api = RetrofitEngine.retrofit
    private lateinit var sessionManager: SessionManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        toolbarTitle.text = "Profile"
        sessionManager = SessionManager(this)
        clickListeners()
        getCustomerDetails()
        ImagePickerActivity.clearCache(this)
    }
    private fun clickListeners() {
        imge_profile.setOnClickListener {
            onProfileImageClick()
        }
        okButton.setOnClickListener {
            when{
                emp_no.text.isNullOrEmpty()->{
                    emp_no.error = "Please enter User Name"
                }
                emp_no.text.isNullOrEmpty()->{
                    emp_no.error = "Please enter Address"
                }
                else -> {
                    profileUpdate()
                }
            }
        }
    }
    fun onProfileImageClick() {
        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (report!!.areAllPermissionsGranted()) {
                        showImagePickerOptions()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<com.karumi.dexter.listener.PermissionRequest>?,
                    token: PermissionToken?
                ) {

                }
            }).check()
    }
    private fun showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, object :
            ImagePickerActivity.PickerOptionListener {
            override fun onTakeCameraSelected() {
                launchCameraIntent()
            }

            override fun onChooseGallerySelected() {
                launchGalleryIntent()
            }
        })
    }
    val REQUEST_IMAGE = 100
    private fun launchCameraIntent() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_IMAGE_CAPTURE
        )

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    private fun launchGalleryIntent() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_GALLERY_IMAGE
        )

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    private fun profileUpdate() {
        CustomProgressbar.showProgressBar(this,false)
        val call: Call<UpdateEmpDetailResponse> = api.updateEmployeeDetails(UpdateEmpDetailsRequest(emp_no.toString(),
            SessionManager(this@ProfileActivity).employeeId.toString(),mobile_no.toString(),
            date.text.toString(),name_id.text.toString() ))
        call.enqueue(object : Callback<UpdateEmpDetailResponse> {
            override fun onResponse(call: Call<UpdateEmpDetailResponse>, response: Response<UpdateEmpDetailResponse>) {
                CustomProgressbar.hideProgressBar()
                val updateCustomerResponse = response.body()
                if (updateCustomerResponse != null) {
                    if (updateCustomerResponse.status?.equals(200)) {
                        Toast.makeText(this@ProfileActivity, "Profile Updated", Toast.LENGTH_LONG).show()
                        uploadProfile()
                    } else {
                        Toast.makeText(this@ProfileActivity, "Profile Update Failed", Toast.LENGTH_LONG).show()
                    }
                }
            }

            override fun onFailure(call: Call<UpdateEmpDetailResponse>, t: Throwable) {
                CustomProgressbar.hideProgressBar()
                Toast.makeText(this@ProfileActivity,getString(R.string.request_failed), Toast.LENGTH_LONG).show();
            }
        })
    }
    private fun getCustomerDetails() {
        CustomProgressbar.showProgressBar(this,false)
        val call: Call<GetEmployeeDetailResponse> = api.getEmployeeDetails(GetEmployeeDetailsRequest( SessionManager(this@ProfileActivity).employeeId.toString()))
        call.enqueue(object : Callback<GetEmployeeDetailResponse> {
            override fun onResponse(call: Call<GetEmployeeDetailResponse>, response: Response<GetEmployeeDetailResponse>) {
                CustomProgressbar.hideProgressBar()
                val getEmployeeDetailResponse = response.body()
                if (getEmployeeDetailResponse != null) {
                    if (getEmployeeDetailResponse.status?.equals(200)) {
                        address_id.setText(getEmployeeDetailResponse.address)
                        date.setText(getEmployeeDetailResponse.joiningDate)
                        name_id.setText(getEmployeeDetailResponse.name)
                        emp_no.setText(getEmployeeDetailResponse.employeeNo)
                        email_id.setText(getEmployeeDetailResponse.email)
                        mobile_no.setText(getEmployeeDetailResponse.mobile)
                        Glide.with(this@ProfileActivity).load(getEmployeeDetailResponse.image)
                            .into(imge_profile)
                    } else {
                        Toast.makeText(this@ProfileActivity, "Profile Update Failed", Toast.LENGTH_LONG).show()
                    }
                }
            }

            override fun onFailure(call: Call<GetEmployeeDetailResponse>, t: Throwable) {
                CustomProgressbar.hideProgressBar()
                Toast.makeText(this@ProfileActivity,getString(R.string.request_failed), Toast.LENGTH_LONG).show();
            }
        })
    }
    var fileUri : Uri? = null
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data!!.getParcelableExtra<Uri>("path")
                try {
                    // You can update this bitmap to your server
                    val bitmap =
                        MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
                    fileUri = uri
                    Glide.with(this).load(uri)
                        .into(imge_profile)
                    imge_profile.visibility = View.VISIBLE
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }
    private fun uploadProfile() {
        if(fileUri!=null){
            val file = FileUtils.getFile(this,fileUri)
            file?.let {
                val stringForm = "multipart/form-data".toMediaTypeOrNull()
                val customer: RequestBody = SessionManager(this@ProfileActivity).employeeId.toString().toRequestBody(stringForm)
                val mapImages: MutableMap<String, RequestBody> = HashMap()
                val requestFile: RequestBody = file.asRequestBody(stringForm)
                mapImages["image\"; filename=\"" + file.name] = requestFile
                CustomProgressbar.showProgressBar(this, false)
                CoroutineScope(Dispatchers.IO).launch {
                    val request = api.uploadProfile(mapImages,customer)
                    withContext(Dispatchers.Main) {
                        try {
                            val response = request.await()
                            if (response.isSuccessful) {
                                CustomProgressbar.hideProgressBar()
                                onBackPressed()
                            } else {
                                CustomProgressbar.hideProgressBar()
                            }
                        } catch (e: HttpException) {
                            e.printStackTrace()
                        } catch (e: Throwable) {
                            e.printStackTrace()
                        }
                    }
                }
            }
        }else{
            onBackPressed()
        }
    }
}