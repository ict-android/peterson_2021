package com.petersonactivity.ui.Attendance

import android.os.Bundle
import com.petersonactivity.R
import com.petersonactivity.ui.Base.BaseActivity
import kotlinx.android.synthetic.main.toolbar.*

class AttendanceActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_attendance)
        toolbarTitle.text = "ATTENDANCE"
    }
}