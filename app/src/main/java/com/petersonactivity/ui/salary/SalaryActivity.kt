package com.petersonactivity.ui.salary

import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.smsautolineactivity.data_models.SessionManager
import com.app.smsautolineactivity.service.RetrofitEngine
import com.app.smsautolineactivity.utils.AppLogger
import com.app.smsautolineactivity.utils.CustomProgressbar
import com.bumptech.glide.Glide
import com.petersonactivity.R
import com.petersonactivity.request.GetSalaryBalanceRequest
import com.petersonactivity.ui.Base.BaseActivity
import com.petersonactivity.request.SalaryDetailsRequest
import com.petersonactivity.response.GetSalaryBalanceResponse
import com.petersonactivity.response.MonthlyData
import kotlinx.android.synthetic.main.activity_listcollection.*
import kotlinx.android.synthetic.main.activity_password_change.*
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.activity_salary.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SalaryActivity : BaseActivity() {

    private var api = RetrofitEngine.retrofit
    val dataSetList = ArrayList<MonthlyData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_salary)
        toolbarTitle.text = "SALARY DETAILS"
        setUpRecycler()
        getSalaryDetails()
        getSalaryBalance()

    }
    private fun setUpRecycler() {
        recycler_sal.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycler_sal.adapter = SalaryDetailsAdapter(dataSetList, this)
        (recycler_sal.adapter as SalaryDetailsAdapter).notifyDataSetChanged()
    }
    private fun getSalaryDetails() {
        CustomProgressbar.showProgressBar(this, false)
        CoroutineScope(Dispatchers.IO).launch {
            SessionManager(this@SalaryActivity).employeeId?.let { cu->
                val request = api.getEmployeeSalaryDetails(SalaryDetailsRequest(cu))
                withContext(Dispatchers.Main) {
                    try {
                        val response = request.await()
                        if (response.isSuccessful) {
                            CustomProgressbar.hideProgressBar()
                            response.body()?.let {
                                AppLogger.e("ResponseData", it.toString())
                                dataSetList.clear()
                                it.salary?.forEach { salary ->
                                    salary.monthlyData?.forEach { monthlyData ->
                                        dataSetList.add(monthlyData)
                                    }
                                }
                                (recycler_sal.adapter as SalaryDetailsAdapter).notifyDataSetChanged()
                            }
                        } else {
                            CustomProgressbar.hideProgressBar()
                        }
                    } catch (e:Exception ) {
                        e.printStackTrace()
                    }
                }
            }
        }

    }
    private fun getSalaryBalance( ) {
        CustomProgressbar.showProgressBar(this,false)
        val call: Call<GetSalaryBalanceResponse> = api.getSalaryBalance(
            GetSalaryBalanceRequest(SessionManager(this@SalaryActivity).employeeId.toString())
        )
        call.enqueue(object : Callback<GetSalaryBalanceResponse> {
            override fun onResponse(call: Call<GetSalaryBalanceResponse>, response: Response<GetSalaryBalanceResponse>) {
                CustomProgressbar.hideProgressBar()
                val getSalaryBalanceResponse = response.body()
                if (getSalaryBalanceResponse != null) {
                    if (getSalaryBalanceResponse.status?.equals(200)) {
                        salary_pending_amt.text= getSalaryBalanceResponse.salaryBalance.toString()

                    }

                    if (getSalaryBalanceResponse.message.contains("Success")) {
                        Toast.makeText(this@SalaryActivity, getSalaryBalanceResponse.message, Toast.LENGTH_LONG).show()

                    } else {
                        Toast.makeText(this@SalaryActivity, getSalaryBalanceResponse.message, Toast.LENGTH_LONG).show()
                    }
                }else {
                    Toast.makeText(this@SalaryActivity, "", Toast.LENGTH_LONG).show()
                }
            }

            override fun onFailure(call: Call<GetSalaryBalanceResponse>, t: Throwable) {
                CustomProgressbar.hideProgressBar()
                Toast.makeText(this@SalaryActivity,getString(R.string.request_failed), Toast.LENGTH_LONG).show();
            }
        })
    }



}