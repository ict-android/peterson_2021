package com.petersonactivity.ui.salary

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.petersonactivity.R
import com.petersonactivity.response.MonthlyData
import com.petersonactivity.response.Salary
import kotlinx.android.synthetic.main.salary_detail1.view.*
import kotlinx.android.synthetic.main.salary_details.view.*
import kotlinx.android.synthetic.main.salary_details.view.month_txt

class SalaryDetailsAdapter(private val myDataset: ArrayList<MonthlyData>, val context: Context) :
    RecyclerView.Adapter<SalaryDetailsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.salary_detail1, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataInfo = myDataset[position]
        holder.month.text = dataInfo.description
        holder.date.text = dataInfo.date
        holder.inAmount.text = dataInfo.inX
        holder.outAmount.text = dataInfo.status

    }
    override fun getItemCount() = myDataset.size

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val month = view.month_txt_id
        val date =view.month_date
        val inAmount =view.amt_balance
        val outAmount =view.Salary_detail

    }
}