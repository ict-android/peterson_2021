package com.petersonactivity.ui

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.petersonactivity.R
import com.petersonactivity.request.Custom
import kotlinx.android.synthetic.main.dashboard.view.*


class CustomAdapter(private val context: Context, private val homeContentList: ArrayList<Custom>) :
    RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.dashboard, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataInfo = homeContentList[position]
        holder.textViewName.text = dataInfo.name
        holder.imageView.setImageResource(dataInfo.Image)
        holder.homeIconLayout.setOnClickListener {
            dataInfo.cls?.let {
                context.startActivity(Intent(context,it))
            }
        }
    }

    override fun getItemCount(): Int {
        return homeContentList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewName = itemView.txt_profile
        val imageView=itemView.login_profile
        val homeIconLayout=itemView.homeIconLayout
    }
}