package com.petersonactivity.ui.Order

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.app.smsautolineactivity.service.RetrofitEngine
import com.bumptech.glide.Glide
import com.petersonactivity.R
import com.petersonactivity.response.Order
import com.petersonactivity.ui.Base.BaseActivity
import kotlinx.android.synthetic.main.activity_order_detail.*
import kotlinx.android.synthetic.main.toolbar.*

class OrderDetailActivity : BaseActivity(){

    private var api = RetrofitEngine.retrofit
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_detail)
        toolbarTitle.text = "Order Details"
        val complaint : Order? =intent.getParcelableExtra("orderData")
        complaint?.let {
            name_id.text = it.customerName
            order_details.text = it.orderNo
            date_ti.text = it.date
            oredr_amt.text = it.orderAmount
            date_ti.text = it.date
            bal_amt.text = it.balanceAmount
            receive_amt.text = it.receivedAmount
            color_id.text=it.color
            squre_ft.text=it.workSqFeet
            description_id.text=it.description
            per_sq_ft.text=it.perSqFeetAmt
            work_sts.text=it.status
            advance_amount.text=it.advanceAmount
            disc_amt.text = it.discountAmount

            }
        clickListener()
        }
    private fun clickListener() {
        okButton.setOnClickListener {
            onBackPressed()
        }
    }
}