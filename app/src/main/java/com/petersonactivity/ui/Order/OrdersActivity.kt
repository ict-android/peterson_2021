package com.petersonactivity.ui.Order

import OrderAdapter
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.smsautolineactivity.data_models.SessionManager
import com.app.smsautolineactivity.service.RetrofitEngine
import com.app.smsautolineactivity.utils.AppLogger
import com.app.smsautolineactivity.utils.CustomProgressbar
import com.petersonactivity.R
import com.petersonactivity.request.OrderListRequest
import com.petersonactivity.request.SalaryDetailsRequest
import com.petersonactivity.response.Order
import com.petersonactivity.ui.Base.BaseActivity
import kotlinx.android.synthetic.main.activity_listcollection.*
import kotlinx.android.synthetic.main.activity_listcollection.noDataFound
import kotlinx.android.synthetic.main.activity_orders.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class OrdersActivity : BaseActivity() {

    private var api = RetrofitEngine.retrofit
    val dataSetList = ArrayList<Order>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_orders)
        toolbarTitle.text = "Order List"
        setUpRecycler()
    }

    override fun onResume() {
        super.onResume()
        getEmployeeOrders()
    }


    private fun setUpRecycler() {
        recycler_order.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycler_order.adapter = OrderAdapter(dataSetList, this)
        (recycler_order.adapter as OrderAdapter).notifyDataSetChanged()
    }

    private fun getEmployeeOrders() {
        CustomProgressbar.showProgressBar(this, false)
        CoroutineScope(Dispatchers.IO).launch {
            SessionManager(this@OrdersActivity).employeeId?.let { cu->
                val request = api.getOrderDetails(OrderListRequest(cu))
                withContext(Dispatchers.Main) {
                    try {
                        val response = request.await()
                        if (response.isSuccessful) {
                            CustomProgressbar.hideProgressBar()
                            response.body()?.let {
                                AppLogger.e("ResponseData", it.toString())
                                it.orders?.let{orders->
                                    if(orders.isEmpty()){
                                        noDataFound.visibility = View.VISIBLE
                                    } else {
                                        noDataFound.visibility = View.GONE
                                        dataSetList.clear()
                                        dataSetList.addAll(orders)
                                        (recycler_order.adapter as OrderAdapter).notifyDataSetChanged()
                                    }
                                }
                            }
                        } else {
                            noDataFound.visibility = View.VISIBLE
                            CustomProgressbar.hideProgressBar()
                        }
                    } catch (e:Exception ) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }
}