import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.petersonactivity.R
import com.petersonactivity.response.Collection
import com.petersonactivity.response.Order
import com.petersonactivity.ui.Order.OrderDetailActivity
import com.petersonactivity.ui.collections.EditCollectionActivity
import kotlinx.android.synthetic.main.list_collection.view.*
import kotlinx.android.synthetic.main.list_collection.view.layout
import kotlinx.android.synthetic.main.order_list.view.*

class OrderAdapter(private val myDataset: ArrayList<Order>, val context: Context) :
    RecyclerView.Adapter<OrderAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.order_list, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataInfo = myDataset[position]
        holder.name.text="Name:  ${dataInfo.customerName}"
        holder.orderNo.text ="Order No: ${dataInfo.orderNo}"
        holder.date.text = "Date : ${dataInfo.date}"
        holder.orderAmount.text = "Order Amount : ${dataInfo.orderAmount}"
        holder.layout.setOnClickListener {
            val intentVar = Intent(context, OrderDetailActivity::class.java)
            intentVar.putExtra("orderData",dataInfo)
            context.startActivity(intentVar)
        }
    }

    override fun getItemCount() = myDataset.size

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {

        val orderNo = view.order_no_l
        val name=view.order_name
        val orderAmount =view.amount_order
        val date =view.date_id_order
        val layout=view.layout_order
    }
}