package com.app.smsautolineactivity.data_models

import android.content.Context
import com.google.gson.Gson

class SessionManager(private val context: Context) {
    private val gson: Gson
    fun clear() {
        val editor =
            context.getSharedPreferences("Peterson", Context.MODE_PRIVATE).edit()
        editor.clear()
        editor.apply()
    }

    fun saveUserInfo(email: String?, employeeId : String?,address:String?,mobile:String?,employeeName:String?) {
        val editor =
            context.getSharedPreferences("Peterson", Context.MODE_PRIVATE).edit()
        editor.putString("email", email)
        editor.putString("empId", employeeId)
        editor.putString("address", address)
        editor.putString("mobile", mobile)
        editor.putString("empname", employeeName)

        editor.apply()
    }


    val employeeId: String?
        get() {
            val prefs =
                context.getSharedPreferences("Peterson", Context.MODE_PRIVATE)
            return prefs.getString("empId", null)
        }
    val address: String?
        get() {
            val prefs =
                context.getSharedPreferences("Autoline_SMS", Context.MODE_PRIVATE)
            return prefs.getString("address", null)
        }
    val mobile: String?
        get() {
            val prefs =
                context.getSharedPreferences("Autoline_SMS", Context.MODE_PRIVATE)
            return prefs.getString("mobile", null)
        }
    val customerName: String?
        get() {
            val prefs =
                context.getSharedPreferences("Autoline_SMS", Context.MODE_PRIVATE)
            return prefs.getString("customerName", null)
        }
    val equipmentId: String?
        get() {
            val prefs =
                context.getSharedPreferences("Autoline_SMS", Context.MODE_PRIVATE)
            return prefs.getString("equipmentId", null)
        }

    fun saveComplaintInfo(contactPerson: String?, customerId : String?,address : String?,contactNumber : String?,
                          complaint: String?,remarks:String?,equipmentId: String?) {
        val editor =
            context.getSharedPreferences("Autoline_SMS", Context.MODE_PRIVATE).edit()
        editor.putString("contactPerson", contactPerson)
        editor.putString("customerId", customerId)
        editor.putString("address", address)
        editor.putString("contactNumber", contactNumber)
        editor.putString("complaint", complaint)
        editor.putString("remarks", remarks)
        editor.putString("equipmentId", equipmentId)
        editor.apply()
    }
    init {
        gson = Gson()
    }
}

