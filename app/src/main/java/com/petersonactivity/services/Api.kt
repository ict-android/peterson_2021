package com.app.smsautolineactivity.service



import com.petersonactivity.request.*
import com.petersonactivity.response.*
import kotlinx.coroutines.Deferred
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface Api {

    @POST("isValidEmployee")
    fun doLogin(@Body loginRequest: LoginRequest): Call<LoginResponse>

    @POST("isValidEmployee")
    fun changeEmployeePassword(@Body changeEmployeePasswordRequest: ChangeEmployeePasswordRequest): Call<ChangeEmployeePasswordResponse>

    @POST("getEmployeeSalaryBalance")
    fun getSalaryBalance(@Body getSalaryBalanceRequest: GetSalaryBalanceRequest):Call<GetSalaryBalanceResponse>


    @POST("getEmployeeDetails")
    fun getEmployeeDetails(@Body getEmployeeDetailsRequest: GetEmployeeDetailsRequest):Call<GetEmployeeDetailResponse>

    @POST("updateEmployeeDetails")
    fun updateEmployeeDetails(@Body updateEmployeeDetails: UpdateEmpDetailsRequest):Call<UpdateEmpDetailResponse>

    @POST("getEmployeeSalaryDetails")
    fun getEmployeeSalaryDetails(@Body salaryDetailsRequest: SalaryDetailsRequest): Deferred<Response<SalaryDetailsResponse>>


    @POST("getEmployeeOrders")
    fun getEmployeeOrders(@Body salaryDetailsRequest: SalaryDetailsRequest): Deferred<Response<EmployeeOrders>>

    @POST("getEmployeeAttendance")
    fun getEmployeeAttendance(@Body attendanceRequest: AttendanceRequest): Call<AttendanceResponse>


    @POST("addCollection")
    fun addCollection(@Body addCollectionRequest: AddCollectionRequest): Call<AddCollectionResponse>

    @Multipart
    @POST("uploadEmployeeImage")
    fun uploadProfile(
        @PartMap file: Map<String, @JvmSuppressWildcards RequestBody>,
        @Part("employee_id") employee_id: RequestBody
    ): Deferred<Response<GetEmployeeDetailResponse>>

    @POST("getEmployeeOrders")
    fun getOrderDetails(@Body orderListRequest: OrderListRequest):  Deferred<Response<OrderListResponse>>

    @POST("updateCollection")
    fun updateCollection(@Body updateCollectionRequest: UpdateCollectionRequest): Call<UpdateCollectionResponse>

    @POST("getCollectionDetails")
    fun getCollectionDetails(@Body getCollectionDetailsRequest: GetCollectionDetailsRequest): Call<GetCollectionDetailsResponse>

    @POST("getAllCollections")
    fun getAllCollections(@Body getAllCollectionRequest: GetAllCollectionRequest): Deferred<Response<GetAllCollectionResponse>>
}

